# .NET Docker images

[![License: MIT][project-license-badge]][project-license]
[![Donate][paypal-donations-badge]][paypal-donations]
[![standard-readme compliant][github-standard-readme-badge]][github-standard-readme]
[![GitLab pipeline status][gitlab-pipeline-status-badge]][gitlab-pipelines]
[![Docker Stars][docker-stars-badge]][docker-repository]
[![Docker Pulls][docker-pulls-badge]][docker-repository]

Docker images with .NET Core, based on Debian Linux.

Images are based on [official dotnet images][docker-microsoft-dotnet]
and are enriched with other useful packages and configurations.
If a dotnet LTS image for a specific major version is available,
then it is preferred over newer versions.

Runtime images follow a stricter security approach than [official dotnet images][docker-microsoft-dotnet],
as they are configured to run with an unprivileged user.

## Table of Contents

- [Tags](#tags)
- [Usage](#usage)
  - [Security](#security)
  - [Azure App Service](#azure-app-service)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
  - [Building Docker images](#building-docker-images)
- [License](#license)

## Tags

Following tags are available on [Docker Hub][docker-repository]:

| Tag name         | WORKDIR    | dotnet base image | Node.js | End of life |
|------------------|------------|-------------------|---------|-------------|
| `latest`         | `/opt/sln` | `sdk:7.0`         | `18.x`  | 05/2024     |
| `7-aspnet`       | `/opt/app` | `aspnet:7.0`      |         | 05/2024     |
| `7-azure-appsvc` | `/opt/app` | `aspnet:7.0`      |         | 05/2024     |
| `7-sdk`          | `/opt/sln` | `sdk:7.0`         | `18.x`  | 05/2024     |
| `6-aspnet`       | `/opt/app` | `aspnet:6.0`      |         | 11/2024     |
| `6-azure-appsvc` | `/opt/app` | `aspnet:6.0`      |         | 11/2024     |
| `6-sdk`          | `/opt/sln` | `sdk:6.0`         | `16.x`  | 11/2024     |

Tags ending with `-sdk` suffix should be used to build .NET Core applications,
while tags ending `-aspnet` and `-azure-appsvc` suffixes should be used at runtime.

`latest` tag is an SDK image which always contains latest stable .NET release.

All SDK images contain the following additions:

- [Node.js][nodejs-website] LTS with build tools.
- [Yarn][yarn-website] latest stable release.
- MySQL and MariaDB command-line client (`mysql`).
- PostgreSQL command-line client (`psql`).
- SQL Server command-line tools (`sqlcmd` and `bcp`).

Tags are rebuilt every week by a job scheduled on GitLab CI platform,
which also performs some automated testing of the image integrity.

## Usage

Provided images are, more or less, a replacement for [official dotnet images][docker-microsoft-dotnet].
As you can see from example `Dockerfile` below:

```docker
FROM pommalabs/dotnet:7-aspnet AS base
# Instead of: mcr.microsoft.com/dotnet/aspnet:7.0

FROM pommalabs/dotnet:7-sdk AS build
# Instead of: mcr.microsoft.com/dotnet/sdk:7.0
COPY ["example.csproj", ""]
RUN dotnet restore "./example.csproj"
COPY . .
WORKDIR "/opt/sln/"
RUN dotnet build "example.csproj" -c Release -o /opt/app/build

FROM build AS publish
RUN dotnet publish "example.csproj" -c Release -o /opt/app/publish /p:UseAppHost=false

FROM base AS final
COPY --from=publish /opt/app/publish .
# Use CMD instead of ENTRYPOINT, which is defined by custom image.
CMD ["dotnet", "example.dll"]
```

The relevant changes are the new tags and the different working directories
(`/opt/sln` for SDK and `opt/app` for runtime). You can also compare it
with the original [Dockerfile generated by Visual Studio 2022][dockerfile-vs2022].

If you want to try the example image for ASP.NET Core, please download the project
and run following commands (I assume that you have Docker installed):

```bash
cd examples/aspnet-core
docker build . -t aspnet-core-example
docker run -it --rm -p 8080:8080 -t aspnet-core-example
```

Browse to `http://localhost:8080/weatherforecast` and you should see an example JSON output,
produced by an ASP.NET Core application running with an unprivileged user.

### Security

Runtime images use by default an unprivileged user (`app`) and they listen on port `8080`,
which does not require admin privileges in order to be used.

### Azure App Service

Azure App Service can run custom Docker images; however, in order to take advantage
of quite useful features, like SSH connection from Azure Portal, custom Docker image should be
[properly configured][azure-appsvc-custom-docker-image].

Tags ending with `-azure-appsvc` suffix are runtime images with the configuration
required to take advantage of those features while preserving a good level of security.

To use them, simply replace `-aspnet` suffix in your base image with `-azure-appsvc` suffix.

Azure App Service images have a custom init script that will:

- Start a SSH server compatible with Azure App Service
  (please check the [official documentation][azure-appsvc-ssh-support-linux]).
- Start the ASP.NET Core application using an unprivileged user.

## Maintainers

[@pomma89][gitlab-pomma89].

## Contributing

MRs accepted.

Small note: If editing the README, please conform to the [standard-readme][github-standard-readme] specification.
I replaced the __Install__ section with __Tags__, since I thought that it made no sense
to "install" an helper Docker image.

### Building Docker images

Docker images can be built with following command:

```bash
while IFS=\| read -r path tag platform
do
    docker build . -f $path/Dockerfile -t $tag
done < ".dockertags"
```

## License

MIT © 2019-2023 [Alessio Parma][personal-website]

[azure-appsvc-custom-docker-image]: https://docs.microsoft.com/en-us/azure/app-service/containers/tutorial-custom-docker-image
[azure-appsvc-ssh-support-linux]: https://docs.microsoft.com/en-us/azure/app-service/containers/app-service-linux-ssh-support
[docker-microsoft-dotnet]: https://hub.docker.com/r/microsoft/dotnet/
[docker-pulls-badge]: https://img.shields.io/docker/pulls/pommalabs/dotnet?style=flat-square
[docker-repository]: https://hub.docker.com/r/pommalabs/dotnet
[docker-stars-badge]: https://img.shields.io/docker/stars/pommalabs/dotnet?style=flat-square
[dockerfile-vs2022]: https://gitlab.com/pommalabs/docker/dotnet/blob/main/examples/aspnet-core/Dockerfile.vs2022
[github-standard-readme]: https://github.com/RichardLitt/standard-readme
[github-standard-readme-badge]: https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square
[gitlab-pipeline-status-badge]: https://gitlab.com/pommalabs/docker/dotnet/badges/main/pipeline.svg?style=flat-square
[gitlab-pipelines]: https://gitlab.com/pommalabs/docker/dotnet/pipelines
[gitlab-pomma89]: https://gitlab.com/pomma89
[nodejs-website]: https://nodejs.org
[paypal-donations]: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=ELJWKEYS9QGKA
[paypal-donations-badge]: https://img.shields.io/badge/Donate-PayPal-important.svg?style=flat-square
[personal-website]: https://alessioparma.xyz/
[project-license]: https://gitlab.com/pommalabs/docker/dotnet/-/blob/main/LICENSE
[project-license-badge]: https://img.shields.io/badge/License-MIT-yellow.svg?style=flat-square
[yarn-website]: https://yarnpkg.com
