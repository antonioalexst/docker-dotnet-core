#!/bin/bash

set -e
source /opt/bash/common_commands.sh

# Create an application user with limited privileges.
useradd --create-home --uid 1000 app

# Create application directory.
mkdir -p /opt/app

# Disable "su" for all users, except those who belong to the root group.
echo "auth required pam_wheel.so" >> /etc/pam.d/su
