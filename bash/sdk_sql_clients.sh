#!/bin/bash

set -e
source /opt/bash/common_commands.sh

# Install MySQL and MariaDB client (mysql).
$SLIM_INSTALL mariadb-client
mysql --version

# Install PostgreSQL client (psql).
$SLIM_INSTALL postgresql-client
psql --version

# Install SQL Server command-line tools (sqlcmd, bcp).
$SAFE_CURL https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
$SAFE_CURL https://packages.microsoft.com/config/$MS_REPO_DISTRO/prod.list | tee /etc/apt/sources.list.d/msprod.list
apt-get update -qq
ACCEPT_EULA=Y $SLIM_INSTALL mssql-tools unixodbc-dev

# Localization fix for sqlcmd.
$SLIM_INSTALL locales
echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
DEBIAN_FRONTEND=noninteractive locale-gen
sqlcmd -?
