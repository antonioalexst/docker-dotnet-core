#!/bin/bash

set -e
source /opt/bash/common_commands.sh

# Install dumb-init (https://github.com/Yelp/dumb-init).
$SLIM_INSTALL dumb-init
dumb-init --version
