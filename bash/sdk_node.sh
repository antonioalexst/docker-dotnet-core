#!/bin/bash

set -e
source /opt/bash/common_commands.sh

# Install Node.js LTS with build tools.
$SAFE_CURL https://deb.nodesource.com/setup_$NODE_VERSION | bash -
$SLIM_INSTALL build-essential nodejs
node --version && npm --version

# Install Yarn.
npm install --global yarn
yarn --version
