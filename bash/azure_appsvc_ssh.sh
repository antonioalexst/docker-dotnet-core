#!/bin/bash

set -e
source /opt/bash/common_commands.sh

# Setup APT packages.
$SLIM_INSTALL openssh-server

# Set root password for SSH access.
echo "root:Docker!" | chpasswd

# Create SSH keys.
ssh-keygen -A

# Make sure SSH run directory exists.
mkdir -p /var/run/sshd
