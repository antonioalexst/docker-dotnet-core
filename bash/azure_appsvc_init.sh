#!/bin/bash

set -e

# Start SSH server, which will be used to connect
# to the running container via Azure Portal.
sed -i "s/SSH_PORT/$SSH_PORT/g" /etc/ssh/sshd_config
/usr/sbin/sshd 

# Start application server as unprivileged "app" user.
su app -c "$*"
