#!/bin/bash

set -e
source /opt/bash/common_commands.sh

# Setup APT repositories.
apt-get update -qq

# Setup GDI+, required by System.Drawing.
$SLIM_INSTALL libgdiplus
