ARG SUFFIX=""
FROM pommalabs/dotnet:7-aspnet${SUFFIX}

# Switch back to root user, Azure App Service SSH blade
# requires an active SSH server running (which in turn
# requires root user in order to be started).
USER root

ARG description="Docker image with ASP.NET Core for Azure App Service, based on Debian Linux"
LABEL description=${description} org.opencontainers.image.description=${description}

# Use ports which do not require root user.
EXPOSE 8080 2222

ENV SSH_PORT=2222

# Copy Azure App Service SSH configuration.
COPY azure-appsvc/sshd_config /etc/ssh/sshd_config

# Copy Azure App Service init script.
COPY ./bash/azure_appsvc_init.sh /usr/local/sbin/azure-appsvc-init
RUN chmod 500 /usr/local/sbin/azure-appsvc-init

# Custom scripts.
COPY ./bash /opt/bash
RUN bash /opt/bash/common_setup.sh && \
    bash /opt/bash/azure_appsvc_ssh.sh && \
    bash /opt/bash/common_cleanup.sh && \
    rm -rf /opt/bash

# Run dumb-init as main process (https://github.com/Yelp/dumb-init),
# followed by custom Azure App Service init.
ENTRYPOINT ["dumb-init", "--", "azure-appsvc-init"]
